//
//  RequestDeliveryViewController.m
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "RequestDeliveryViewController.h"
#import "ParserHandler.h"
#import "SDViewController.h"

@interface RequestDeliveryViewController ()
{
    NSArray * arr;
    NSArray * arr_payment;
    NSString * script;
    float deviceFactor;
    UIActivityIndicatorView * spinner;
}
@end

@implementation RequestDeliveryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Request Delivery";
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    deviceFactor = IS_IPHONE_4?1:1.1833;
    //float deviceFactor=IS_IPHONE_4?1:1.1833;
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed: IS_IPHONE_4?@"back_i4":@"back1_i5"]]];
    
    [super viewDidLoad];
    arr = [[NSArray alloc]initWithObjects:@"Small",@"Medium",@"Big", nil];
    arr_payment = [[NSArray alloc]initWithObjects:@"Cash",@"Coupan", nil];
    [self addLabelWithFrame:CGRectMake(110.0, 157.5*deviceFactor, 100.0, 20.0) tag:101 title:@"ID:"];
    [self addLabelWithFrame:CGRectMake(40.0, 207.5*deviceFactor, 100.0, 20.0) tag:102 title:@"Bottle Type:"];
    [self addLabelWithFrame:CGRectMake(60.0 ,257.5*deviceFactor, 100.0, 20.0) tag:103 title:@"Quantity:"];
    [self addLabelWithFrame:CGRectMake(20.0, 307.5*deviceFactor, 130.0, 20.0) tag:104 title:@"Payment Type:"];
    
    [self addTextFieldWithFrame:CGRectMake(140.0, 157.5*deviceFactor, 150.0, 25.0) tag:201 title:@""];
    [self addTextFieldWithFrame:CGRectMake(140.0, 207.5*deviceFactor, 150.0, 25.0) tag:204 title:@""];
    //[self addTextFieldWithFrame:CGRectMake(160.0, 130.0, 130.0, 20.0) tag:202 title:@""];
    [self addTextFieldWithFrame:CGRectMake(140.0, 257.5*deviceFactor, 150.0, 25.0) tag:203 title:@""];
    //[self addTextFieldWithFrame:CGRectMake(160.0, 210.0, 130.0, 20.0) tag:204 title:@""];
    [self addTextFieldWithFrame:CGRectMake(140.0, 307.0*deviceFactor, 150.0, 25.0) tag:205 title:@""];
    
    UIImage *saveImg=[UIImage imageNamed:IS_IPHONE_4? @"save_i4":@"save_i5"];
    UIImage * cancelImg = [UIImage imageNamed:IS_IPHONE_4? @"cancel_i4":@"cancel_i5"];
    //myscroll = [[UIScrollView alloc] init];

    [self addButtonWithFrame:CGRectMake(20.0, 400.0*deviceFactor, saveImg.size.width, saveImg.size.height) tag:301 title:@"" image:saveImg];
    [self addButtonWithFrame:CGRectMake(170.0, 400.0*deviceFactor, cancelImg.size.width, cancelImg.size.height) tag:302 title:@"" image:cancelImg];
    
    [self addButtonWithFrame:CGRectMake(260.0, 207.5*deviceFactor, 30.0, 25.0) tag:303 title:@"" image:[UIImage imageNamed:@"drop"]];
    [self addButtonWithFrame:CGRectMake(260.0, 307.0*deviceFactor, 30.0, 25.0) tag:304 title:@"" image:[UIImage imageNamed:@"drop"]];
    
    //myscroll.frame = self.view.bounds;
    //myscroll.contentSize = CGSizeMake(400, 800);
    //scroll view size
    //myScroll.showsVerticalScrollIndicator = NO;    // to hide scroll indicators!
    
   // myScroll.showsHorizontalScrollIndicator = YES; //by default, it shows!
   // myscroll.scrollEnabled = YES;                 //say "NO" to disable scroll
   // [self.view addSubview:myscroll];
    
    // Do any additional setup after loading the view.
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel * label = [[UILabel alloc]initWithFrame:frame];
    [label setText:title];
    label.tag = tag;
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    [label setTextColor: [UIColor blackColor]];
    [self.view addSubview: label];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*) title image:(UIImage *)image
{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [self.view addSubview:button];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if (sender.tag==203||sender.tag== 205)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y-130,self.view.frame.size.width,self.view.frame.size.height)];
    }
    if (sender.tag ==201 ||sender.tag ==203|| sender.tag ==204|| sender.tag ==205)
    {
        UITableView * drop_tbl = (UITableView *)[self.view viewWithTag:601];
        [drop_tbl removeFromSuperview];
        
        UITableView * drop_tbl1 = (UITableView *)[self.view viewWithTag:602];
        [drop_tbl1 removeFromSuperview];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if (textField.tag==204||textField.tag ==205)
    {
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)sender
{
    if (sender.tag==205||sender.tag== 203)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+130,self.view.frame.size.width,self.view.frame.size.height)];
    }
}

-(IBAction)buttonAction:(UIButton *)sender
{
    [self.view endEditing:YES];
    if (sender.tag ==301)
    {
        UITextField * idTf = (UITextField *)[self.view viewWithTag:201];
        UITextField * bottleTf = (UITextField *)[self.view viewWithTag:203];
        UITextField * payment_Tf = (UITextField *)[self.view viewWithTag:205];
        //UIButton * btypeTf = (UIButton *)[self.view viewWithTag:303];
        UITextField * qty = (UITextField *) [self.view viewWithTag:204];
       // UIButton * paymentTf = (UIButton *)[self.view viewWithTag:304];
        if (idTf.text.length ==0|| bottleTf.text.length ==0|| payment_Tf.text.length ==0|| qty.text.length ==0)
        {
            UIAlertView * alert_requiredfield = [[UIAlertView alloc]initWithTitle:@"Message!!!" message:@"Some fields are missing..." delegate:self cancelButtonTitle:@" Ok " otherButtonTitles:nil];
            [alert_requiredfield show];
            return;
        }
        spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [spinner setCenter:CGPointMake(IS_IPHONE_4?160.0:170.0, IS_IPHONE_4?240.0:290.0)];
        spinner.color=[UIColor blackColor];
        [spinner startAnimating];
        [self.view addSubview:spinner];
        [self.view bringSubviewToFront:spinner];
        
        
        NSLog(@"idTf : %@, btypeTf : %@, qty : %@, paymentTf : %@",idTf.text,bottleTf.text,qty.text,payment_Tf.text);
        ParserHandler * parse = [[ParserHandler alloc]init];
        [parse setServerResponseDelegate:(id)self];
        NSMutableDictionary * reqdict = [[NSMutableDictionary alloc]init];
        [reqdict setObject:@"bookingDetailsSave" forKey:@"task"];
        [reqdict setObject:idTf.text forKey:@"userId"];
        [reqdict setObject:bottleTf.text forKey:@"bottleType"];
        [reqdict setObject:qty.text forKey:@"quantity"];
        [reqdict setObject:payment_Tf.text forKey:@"paymentType"];
        
        [parse serverRequestWithInfo:reqdict];
        
        
    }
    else if (sender.tag ==302)
    {
        SDViewController * firstview = [[SDViewController alloc]init];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if (sender.tag ==303)
    {
        script = @"1";
        [self addTableviewWithFrame:CGRectMake(140.0, IS_IPHONE_4?232.0*deviceFactor:227.0*deviceFactor, 150.0, 60.0) tag:601 title:@""];//drop down list
        UITableView * drop_tbl = (UITableView *)[self.view viewWithTag:602];
        [drop_tbl removeFromSuperview];
    }
    else if (sender.tag ==304)
    {
        script = @"2";
        [self addTableviewWithFrame:CGRectMake(140.0, IS_IPHONE_4?332.0*deviceFactor:327.0*deviceFactor, 150.0, 60.0) tag:602 title:@""];//drop down list
        UITableView * drop_tbl = (UITableView *)[self.view viewWithTag:601];
        [drop_tbl removeFromSuperview];
    }
}
//www.funkiorange.com/Oasis/index.php?task=bookingDetailsSave&userId=10000001&quantity=1&bottleType=Big&paymentType=cash
//www.funkiorange.com/Oasis/index.php?

//NSLog(@"fname : %@, lname : %@, Flat NO : %@, Floor No : %@, Building Name : %@, City : %@, Country : %@",fnameTf.text,lnameTf.text,flatNoTf.text,floorNoTf.text,buildingNameTf.text,CityTf.text,CountryTf.text);

-(void)addTextFieldWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont fontWithName:@"Times New Roman" size:18];
    tf.backgroundColor=[UIColor whiteColor];
    //tf.text=@"Hello World";
    [tf setText:title];
    tf.tag=tag;
    
    [self.view addSubview:tf];
    
    tf.layer.borderColor=[[UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0]CGColor];
    tf.layer.borderWidth= 1.0f;
    [tf setDelegate:(id)self];
}


-(void)addTableviewWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITableView * table_detail = [[UITableView  alloc] initWithFrame:frame style:UITableViewStylePlain];
    table_detail.tag=tag;
    table_detail.dataSource = (id)self;
    table_detail.delegate = (id)self;
    [self.view addSubview:table_detail];
    table_detail.layer.borderColor = [[UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0]CGColor];
    table_detail.layer.borderWidth = 1.0f;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // return number of rows
    if ([script isEqualToString:@"1"])
    {
        return [arr count];
    }
    else
    {
        return [arr_payment count];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString * CellIdentifier = @"Cell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            [cell.textLabel setFont:[UIFont systemFontOfSize:17]];
        }
    if ([script  isEqualToString:@"1"])
    {
        cell.textLabel.text = [arr objectAtIndex:indexPath.row];
    }
    else if([script isEqualToString:@"2"])
    {
        cell.textLabel.text = [arr_payment objectAtIndex:indexPath.row];
    }
    return cell;
    // return cell
}

- (void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // NSLog(@"didselect %@",[arr objectAtIndex:indexPath.row]);
    if ([script isEqualToString:@"1"])
    {
        UITextField * txt_btype = (UITextField *)[self.view viewWithTag:204];
        [txt_btype setText:[arr objectAtIndex:indexPath.row]];
        [tableView1 removeFromSuperview];
        //ck
    }
    else
    {
        UITextField * txt_btype = (UITextField *)[self.view viewWithTag:205];
        [txt_btype setText:[arr_payment objectAtIndex:indexPath.row]];
        [tableView1 removeFromSuperview];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
   
    UITableView * drop_tbl = (UITableView *)[self.view viewWithTag:601];
    [drop_tbl removeFromSuperview];
    
    UITableView * drop_tbl1 = (UITableView *)[self.view viewWithTag:602];
    [drop_tbl1 removeFromSuperview];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark ServerResponseDelegate

-(void)didRecieveResponseFromServer:(id)response12
{
    if ([[[response12 valueForKey:@"response"]valueForKey:@"success"]isEqualToString:@"true"])
    {
        NSLog(@"didRecieveResponse %@",response12);
        [spinner stopAnimating];
        //NSLog(@"reskipnse scla %@ %@",[response12 class],[spinner class]);
         NSLog(@"%@",[[response12 valueForKey:@"response"]valueForKey:@"userInformation"]);
        UIAlertView * alert_requestdelivery = [[UIAlertView alloc]initWithTitle:@"Message!!!" message:[[response12 valueForKey:@"response"]valueForKey:@"userInformation"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert_requestdelivery show];
        [spinner stopAnimating];
    }
    else
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Message" message:[[response12 valueForKey:@"response"]valueForKey:@"error"]delegate:self cancelButtonTitle:@"Ok " otherButtonTitles:nil];
        [alert show];
        [spinner stopAnimating];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        
        if([title isEqualToString:@"Ok"])
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if([title isEqualToString:@"Ok "])
        {
            
        }
    else if ([title isEqualToString:@" Ok"])
    {
        
    }
}
//       [self.navigationController popToRootViewControllerAnimated:YES];
        //SDViewController * first = [[SDViewController alloc]init];
        //[self.navigationController pushViewController:first animated:YES];
    


-(void)didFailWithErrorFromServer:(NSError*)err
{
    NSLog(@"didFailWithError %@",err);
    
    UIAlertView  * alert_request_error = [[UIAlertView alloc]initWithTitle:@"Error!!!" message:err.localizedDescription delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert_request_error show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

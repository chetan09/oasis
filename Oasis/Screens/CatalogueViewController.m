//
//  CatalogueViewController.m
//  Oasis
//
//  Created by Ravi kumar on 07/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "CatalogueViewController.h"
#import "SDViewController.h"

@interface CatalogueViewController ()

@end

@implementation CatalogueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Catalogue";
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonSystemItemBookmarks target:nil action:nil];
        //[self addLabelWithFrame:CGRectMake(20.0, 150.0, 200.0, 20.0) tag:101 title:@"Work is in Progress..."];
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    float deviceFactor=IS_IPHONE_4?1:1.1833;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:IS_IPHONE_4?@"back_i4":@"back1_i5"]]];
    [self addButtonWithFrame:CGRectMake(200.0, 350.0, 100.0, 30.0) tag:901 title:@"" image:[UIImage imageNamed:@"back11"]];
    
    [self addLabelWithFrame:CGRectMake(20.0, 170.0*deviceFactor, 200.0, 20.0) tag:101 title:@"Work is in Progress..."];
    // Do any additional setup after loading the view.
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel * label = [[UILabel alloc]initWithFrame:frame];
    [label setText:title];
    label.tag = tag;
    [label setTextColor: [UIColor redColor]];
    [self.view addSubview: label];
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*) title
image:(UIImage *)image
{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [self.view addSubview:button];
}

-(IBAction)buttonAction:(UIButton *)sender
{
    if (sender.tag==901)
    {
            SDViewController * firstview = [[SDViewController alloc]init];
            [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

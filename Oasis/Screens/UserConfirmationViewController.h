//
//  UserConfirmationViewController.h
//  Oasis
//
//  Created by Ravi kumar on 05/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserConfirmationViewController : UIViewController<UITextViewDelegate,UIAlertViewDelegate>
@property(nonatomic,strong)NSString *userID;
@end

//
//  UserConfirmationViewController.m
//  Oasis
//
//  Created by Ravi kumar on 05/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "UserConfirmationViewController.h"
#import "SDViewController.h"

@interface UserConfirmationViewController ()
{
    float deviceFactor;
}

@end

@implementation UserConfirmationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Confirmation";
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel * label = [[UILabel alloc]initWithFrame:frame];
    [label setText:title];
    label.tag = tag;
    [label setTextColor: [UIColor redColor]];
    [self.view addSubview: label];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    deviceFactor = IS_IPHONE_4?1:1.1833;
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"smiley1_i4.png"]]];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed: IS_IPHONE_4?@"smiley1_i4":@"smiley1_i5"]]];

    
    //[self addLabelWithFrame:CGRectMake(60.0, 380.0, 400.0, 30.0) tag:501 title:self.userID];
    UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Congratulations!!!" message:self.userID delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    // Do any additional setup after loading the view.
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==0)
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
        //SDViewController * first = [[SDViewController alloc]init];
        //[self.navigationController pushViewController:first animated:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

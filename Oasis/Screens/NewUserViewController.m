//
//  NewUserViewController.m
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "NewUserViewController.h"
#import "ParserHandler.h"
#import "UserConfirmationViewController.h"
#import "SDViewController.h"

@interface NewUserViewController ()
{
    UIActivityIndicatorView * spinner;
}

@end

@implementation NewUserViewController
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        //self.title = @"Registration";
        //self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonSystemItemBookmarks target:nil action:nil];
        
        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    float deviceFactor=IS_IPHONE_4?1:1.1833;
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:IS_IPHONE_4?@"back_i4":@"back1_i5"]]];
    //[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"back"]]];
    
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
     UIImage * save=[UIImage imageNamed:IS_IPHONE_4? @"save_i4":@"save_i5"];
     UIImage * cancel=[UIImage imageNamed:IS_IPHONE_4? @"cancel_i4":@"cancel_i5"];
    
    [self addButtonWithFrame:CGRectMake(20.0, 410.0*deviceFactor, save.size.width, save.size.height) tag:3001 title:@"" image:save];
    [self addButtonWithFrame:CGRectMake(170.0, 410.0*deviceFactor, cancel.size.width, cancel.size.height) tag:3002 title:@"" image:cancel];
    
    //[self addLabelWithFrame:CGRectMake(20.0, 90.0, 100.0, 20.0) tag:1001 title:@"Id No:"];
    [self addLabelWithFrame:CGRectMake(47.0, 150.0*deviceFactor, 100.0, 20.0) tag:1002 title:@"First Name :"];
    [self addLabelWithFrame:CGRectMake(47.0, 180.0*deviceFactor, 100.0, 20.0) tag:1003 title:@"Last Name :"];
    [self addLabelWithFrame:CGRectMake(48.0, 210.0*deviceFactor, 100.0, 20.0) tag:1004 title:@"Locate Me :"];
    [self addLabelWithFrame:CGRectMake(70.0, 240.0*deviceFactor, 100.0, 20.0) tag:1005 title:@"Flat No :"];
    [self addLabelWithFrame:CGRectMake(60.0, 270.0*deviceFactor, 100.0, 20.0) tag:1006 title:@"Floor No :"];
    [self addLabelWithFrame:CGRectMake(14.0, 300.0*deviceFactor, 130.0, 20.0) tag:1006 title:@"Building Name :"];
    [self addLabelWithFrame:CGRectMake(85.0, 330.0*deviceFactor, 50.0, 20.0) tag:1006 title:@"State :"];
    [self addLabelWithFrame:CGRectMake(60.0, 360.0*deviceFactor, 150.0, 20.0) tag:1007 title:@"Country :"];
    
    [self addTextFieldWithFrame:CGRectMake(145.0, 150.0*deviceFactor, 145.0, 25.0) tag:2001 title:@""];
    [self addTextFieldWithFrame:CGRectMake(145.0, 180.0*deviceFactor, 145.0, 25.0) tag:2002 title:@""];
    [self addButtonWithFrame:CGRectMake(270.0, 210.0*deviceFactor, 18.0, 25.0) tag:3003 title:@"" image:[UIImage imageNamed:@"locate_border20x20"]];
    [self addTextFieldWithFrame:CGRectMake(145.0, 210.0*deviceFactor, 125.0, 25.0) tag:1008 title:@"Address...:"];
    
    //[self addTextFieldWithFrame:CGRectMake(160.0, 170.0, 130.0, 20.0) tag:2003 title:@""];
    [self addTextFieldWithFrame:CGRectMake(145.0, 240.0*deviceFactor, 145.0, 25.0) tag:2004 title:@""];
    [self addTextFieldWithFrame:CGRectMake(145.0, 270.0*deviceFactor, 145.0, 25.0) tag:2005 title:@""];
    [self addTextFieldWithFrame:CGRectMake(145.0, 300.0*deviceFactor, 145.0, 25.0) tag:2006 title:@""];
    [self addTextFieldWithFrame:CGRectMake(145.0, 330.0*deviceFactor, 145.0, 25.0) tag:2007 title:@""];
    [self addTextFieldWithFrame:CGRectMake(145.0, 360.0*deviceFactor, 145.0, 25.0) tag:2008 title:@""];
    //[self addTableviewWithFrame:CGRectMake(130.0, 450.0, 130.0, 120.0) tag:5001 title:@""];
    
    //[self addTextFieldWithFrame:CGRectMake(160.0, 410.0, 130.0, 20.0) tag:2008 title:@""];
    
    // Do any additional setup after loading the view.
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];

    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [self.view addSubview:button];
}
-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if (sender.tag==2008||sender.tag== 2007||sender.tag==2006||sender.tag==2005)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y-130,self.view.frame.size.width,self.view.frame.size.height)];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)sender
{
   if (sender.tag==2008||sender.tag== 2007||sender.tag==2006||sender.tag==2005)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+130,self.view.frame.size.width,self.view.frame.size.height)];
    }
}

#pragma mark DropDownViewDelegate

-(void)dropDownCellSelected:(NSInteger)returnIndex
{
    //[btn_click setTitle:[arr_Data objectAtIndex:returnIndex] forState:UIControlStateNormal];
    
}

-(IBAction)buttonAction:(UIButton*)sender1
{
    if (sender1.tag ==3001)
        {
            UITextField *fnameTf=(UITextField*)[self.view viewWithTag:2001];
            UITextField *lnameTf=(UITextField*)[self.view viewWithTag:2002];
            UITextField *flatNoTf=(UITextField*)[self.view viewWithTag:2004];
            UITextField *floorNoTf=(UITextField*)[self.view viewWithTag:2005];
            UITextField *buildingNameTf=(UITextField*)[self.view viewWithTag:2006];
            UITextField *CityTf =(UITextField*)[self.view viewWithTag:2007];
            UITextField *CountryTf =(UITextField*)[self.view viewWithTag:2008];
            if (fnameTf.text.length ==0 || lnameTf.text.length ==0|| flatNoTf.text.length ==0||floorNoTf.text.length ==0|| buildingNameTf.text.length ==0|| CityTf.text.length ==0|| CountryTf.text.length ==0)
            {
                UIAlertView * alert_requiredfield = [[UIAlertView alloc]initWithTitle:@"Message!!!" message:@"Some fields are missing..." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [alert_requiredfield show];
                return;
            }
            spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [spinner setCenter:CGPointMake(IS_IPHONE_4?160.0:170.0,IS_IPHONE_4?240.0:290.0)];
            spinner.color=[UIColor blackColor];
            [spinner startAnimating];
            [self.view addSubview:spinner];
            [self.view bringSubviewToFront:spinner];

            //        UITextField * tf;
            //        if (tf.text.length == 0)
            //        {
            //            <#statements#>
            //        }
            
            //NSLog(@"fname : %@, lname : %@, Flat NO : %@, Floor No : %@, Building Name : %@, City : %@, Country : %@",fnameTf.text,lnameTf.text,flatNoTf.text,floorNoTf.text,buildingNameTf.text,CityTf.text,CountryTf.text);
            
            ParserHandler *parser=[ParserHandler new];
            [parser setServerResponseDelegate:(id)self];
            NSMutableDictionary *infoDictionary=[NSMutableDictionary new];
            [infoDictionary setObject:@"userRegistration" forKey:@"task"];
            [infoDictionary setObject:@"Noida" forKey:@"locateMe"];
            [infoDictionary setObject:fnameTf.text forKey:@"fname"];
            [infoDictionary setObject:lnameTf.text forKey:@"lname"];
            [infoDictionary setObject:flatNoTf.text forKey:@"flatNo"];
            [infoDictionary setObject:floorNoTf.text forKey:@"floorNo"];
            [infoDictionary setObject:buildingNameTf.text forKey:@"buildingName"];
            [infoDictionary setObject:CityTf.text forKey:@"City"];
            [infoDictionary setObject:CountryTf.text forKey:@"Country"];
            [parser serverRequestWithInfo:infoDictionary];
        }
    
        else if (sender1.tag == 3002)
        {
            SDViewController * firstview = [[SDViewController alloc]init];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else if (sender1.tag == 3003)
        {
            spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [spinner setCenter:CGPointMake(IS_IPHONE_4?160.0:170.0, IS_IPHONE_4?240.0:290.0)];
            spinner.color=[UIColor blackColor];
            [spinner startAnimating];
            [self.view addSubview:spinner];
            [self.view bringSubviewToFront:spinner];
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            [locationManager startUpdatingLocation];
            UIButton * btn_loc = (UIButton *)[self.view viewWithTag:3003];
            {
                btn_loc.enabled = false;
            }
        }
}
//ww.funkiorange.com/Oasis/index.php?task=userRegistration&locateMe=noida&fname=sudhir&lname=kushwaha&flatNo=123&floorNo=4&buildingName=dlf%20tower&City=noida&Country=India
-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    label.tag =tag;
    [label setTextColor: [UIColor blackColor]];
    [self.view addSubview: label];
}


-(void)addTextFieldWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UITextField *tf = [[UITextField alloc] initWithFrame:frame];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont fontWithName:@"Times New Roman" size:16];
    tf.backgroundColor=[UIColor whiteColor];
    //tf.text=@"Hello World";
    tf.tag=tag;
    tf.layer.borderColor=[[UIColor colorWithRed:236.0/255 green:236.0/255 blue:236.0/255 alpha:1.0]CGColor];
    tf.layer.borderWidth= 1.0f;
    [tf setDelegate:(id)self];
//    UITextField * _txt_loctn = (UITextField *)[self.view viewWithTag:1008];
//    {
//        UIImageView * imgvw = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"locate-noboder30x20.png"]];
//        [imgvw setFrame:CGRectMake(0, 0, 20, 20)];
//        [_txt_loctn setRightView:imgvw];
//        [_txt_loctn setRightViewMode:UITextFieldViewModeUnlessEditing];
//    }
    
    [self.view addSubview:tf];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
   /* if (currentLocation != nil)
    {
        _lbl_longitude.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        _lbl_latitude.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }*/
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             //UILabel * _lbl_address = (UILabel *)[self.view viewWithTag:1008];
             UITextField * _txt_add = (UITextField *)[self.view viewWithTag:1008];
             _txt_add.text = [NSString stringWithFormat:@"\n%@ %@\n%@\n%@",
                                  placemark.postalCode, placemark.locality,
                                  placemark.administrativeArea,
                                  placemark.country];
             UITextField * _txt_state = (UITextField *)[self.view viewWithTag:2007];
             _txt_state.text = [NSString stringWithFormat:@"%@",placemark.administrativeArea];
             
             UITextField * _txt_country = (UITextField *)[self.view viewWithTag:2008];
             _txt_country.text = [NSString stringWithFormat:@"%@",placemark.country];
             
         }
         else
         {
             NSLog(@"%@", error.debugDescription);
         }
         [spinner stopAnimating];
     } ];
}

#pragma mark ServerResponseDelegate
-(void)didRecieveResponseFromServer:(id)response
{
    NSLog(@"didRecieveResponse %@",response);
    NSDictionary *resp=(NSDictionary*)response;
    NSLog(@"res %@",[[resp valueForKey:@"response"]valueForKey:@"userId"]);
    UserConfirmationViewController * userconfirm = [[UserConfirmationViewController alloc]init];
    userconfirm.userID=[NSString stringWithFormat:@"Your User ID is :%@",[[resp valueForKey:@"response"]valueForKey:@"userId"]];
    [self.navigationController pushViewController:userconfirm animated:YES];
    [spinner stopAnimating];
 
    
    //Chetan
}
-(void)didFailWithErrorFromServer:(NSError*)err
{
    NSLog(@"didFailWithError %@",err);
    UIAlertView * alert_error = [[UIAlertView alloc]initWithTitle:@"Error" message:err.localizedDescription delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
    [alert_error show];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  ParserHandler.m
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "ParserHandler.h"
#define BaseURL @"http://www.funkiorange.com/Oasis/index.php"

@implementation ParserHandler
-(void)serverRequestWithInfo:(NSMutableDictionary *)info
{
    if ([[info valueForKey:@"task"] isEqualToString:@"userRegistration"])
    {
        
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"%@?task=%@&locateMe=%@&fname=%@&lname=%@&flatNo=%@&floorName=%@&buildingName=%@&City=%@&Country=%@",BaseURL,[info valueForKey:@"task"],[info valueForKey:@"locateMe"],[info valueForKey:@"fname"],[info valueForKey:@"lname"],[info valueForKey:@"flatNo"],[info valueForKey:@"floorNo"],[info valueForKey:@"buildingName"],[info valueForKey:@"City"],[info valueForKey:@"Country"]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        [request setHTTPMethod:@"POST"];
        NSLog(@"url %@",request.URL);
        
        //NSString* str;
        //[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURLConnection * connection=[NSURLConnection connectionWithRequest:request delegate:self];
        NSLog(@"%@",connection);
    }
    else
    {
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?task=%@&userId=%@&quantity=%@&bottletype=%@&payment=%@",BaseURL,[info valueForKey:@"task"],[info valueForKey:@"userId"],[info valueForKey:@"bottleType"],[info valueForKey:@"quantity"],[info valueForKey:@"paymentType"]]]];
        [request setHTTPMethod:@"POST"];
        NSLog(@"url %@",request.URL);
        
        NSURLConnection * connection = [NSURLConnection connectionWithRequest:request delegate:self];
    }
}
//www.funkiorange.com/Oasis/index.php?task=userRegistration&locateMe=noida&fname=sudhir&lname=kushwaha&flatNo=123&floorNo=4&buildingName=dlf%20tower&City=noida&Country=India
//www.funkiorange.com/Oasis/index.php?task=bookingDetailsSave&userId=10000001&quantity=1&bottleType=Big&paymentType=cash


#pragma mark NSURLConnection Delegate
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"didfail with error %@",error);
    if ([self.serverResponseDelegate respondsToSelector:@selector(didFailWithErrorFromServer:)]) {
        [self.serverResponseDelegate didFailWithErrorFromServer:error];
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData=[[NSMutableData alloc]init];
    NSLog(@"didrecieve response");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
    NSLog(@"response recieved");
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"didfinishloading");
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:self.responseData options:kNilOptions error:&error];
    
    if (error != nil)
    {
        NSLog(@"Error parsing JSON.");
    }
    else
    {
        
        NSLog(@"Array: %@", jsonDict);
    }
    if ([self.serverResponseDelegate respondsToSelector:@selector(didRecieveResponseFromServer:)])
    {
        [self.serverResponseDelegate didRecieveResponseFromServer:jsonDict];
    }
   
}
@end

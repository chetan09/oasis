//
//  NewUserViewController.h
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>


@interface NewUserViewController : UIViewController<CLLocationManagerDelegate>

@end

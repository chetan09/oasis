//
//  ParserHandler.h
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

@protocol ServerResponseDelegate <NSObject>

-(void)didRecieveResponseFromServer:(id)response;
-(void)didFailWithErrorFromServer:(NSError*)err;

@end


#import <Foundation/Foundation.h>

@interface ParserHandler : NSObject<NSURLConnectionDelegate>
-(void)serverRequestWithInfo:(NSMutableDictionary*)info;
@property(nonatomic,strong)NSMutableData *responseData;
@property (nonatomic, assign) id <ServerResponseDelegate> serverResponseDelegate;
@end

//
//  ContactViewController.m
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "ContactViewController.h"
#import "SDViewController.h"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Contacts";
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    [label setTextColor: [UIColor redColor]];
    [self.view addSubview: label];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.view setBackgroundColor:[UIColor whiteColor]];
    float deviceFactor=IS_IPHONE_4?1:1.1833;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:IS_IPHONE_4?@"back_i4":@"back1_i5"]]];
    
    [self addButtonWithFrame:CGRectMake(200.0, 350.0, 60.0, 20.0) tag:905 title:@"Back" image:[UIImage imageNamed:@"back11"]];
    [self addLabelWithFrame:CGRectMake(60.0, 150.0*deviceFactor, 300.0, 20.0) tag:1006 title:@"Chetan : +919509889734"];
    [self addLabelWithFrame:CGRectMake(60.0, 180.0*deviceFactor, 300.0, 20.0) tag:1006 title:@"Ravi     :  +919876543210"];
    // Do any additional setup after loading the view.
}

-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*) title
image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    [self.view addSubview:button];
}

-(IBAction)buttonAction:(UIButton *)sender
{
    if (sender.tag==905)
    {
        SDViewController * firstview = [[SDViewController alloc]init];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

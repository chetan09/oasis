//
//  main.m
//  Oasis
//
//  Created by Ravi kumar on 08/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDAppDelegate class]));
    }
}

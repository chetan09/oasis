//
//  SDViewController.m
//  Oasis
//
//  Created by Ravi kumar on 04/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import "SDViewController.h"
#import "NewUserViewController.h"
#import "ContactViewController.h"
#import "RequestDeliveryViewController.h"
#import "CatalogueViewController.h"
#import "MyAccountsViewController.h"
#import "PromotionsViewController.h"
#import "HelpViewController.h"

@interface SDViewController ()

@end

@implementation SDViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = @"Oasis";
//        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Back" style:UIBarButtonSystemItemBookmarks target:nil action:nil];        // Custom initialization
    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    float deviceFactor=IS_IPHONE_4?1:1.1833;
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:IS_IPHONE_4?@"background_image_i4":@"background_image_i5"]]];
    
    [self addButtonWithFrame:CGRectMake(IS_IPHONE_4?21.0:21.0, IS_IPHONE_4?153.0*deviceFactor:153.0*deviceFactor, IS_IPHONE_4?130.0:130.0, IS_IPHONE_4?70.0:80.0) tag:1212 title:@"" image:[UIImage imageNamed:IS_IPHONE_4?@"newuser1_i4":@"newuser1_i5"]];
    [self addButtonWithFrame:CGRectMake(170.0, IS_IPHONE_4?153.0*deviceFactor:153.0*deviceFactor, IS_IPHONE_4?130.0:130.0, IS_IPHONE_4?70.0:80.0) tag:1216 title:@"" image:[UIImage imageNamed:IS_IPHONE_4?@"myaccount1_i4":@"myaccount1_i5"]];
    
    [self addButtonWithFrame:CGRectMake(IS_IPHONE_4?21.0:21.0, IS_IPHONE_4?253.0*deviceFactor:253.0*deviceFactor, IS_IPHONE_4?130.0:130.0,IS_IPHONE_4?70.0:80.0) tag:1213 title:@"" image:[UIImage imageNamed:IS_IPHONE_4?@"requestdelivery1_i4":@"request delivery1_i5"]];
    [self addButtonWithFrame:CGRectMake(170.0, IS_IPHONE_4?253.0*deviceFactor:253.0*deviceFactor, IS_IPHONE_4?130.0:130.0, IS_IPHONE_4?70.0:80.0) tag:1216 title:@"" image:[UIImage imageNamed:IS_IPHONE_4?@"promotion1_i4":@"promotion1_i5"]];
    
    [self addButtonWithFrame:CGRectMake(IS_IPHONE_4?21.0:21.0, IS_IPHONE_4?360.0*deviceFactor:365.0*deviceFactor, IS_IPHONE_4?130.0:130.0,30.0) tag:1215 title:@"" image:[UIImage imageNamed:IS_IPHONE_4?@"catalogue_i4":@"catalogue_i5"]];
    [self addButtonWithFrame:CGRectMake(170.0, IS_IPHONE_4?360.0*deviceFactor:365*deviceFactor, IS_IPHONE_4?130.0:130.0, 30.0) tag:1214 title:@"" image:[UIImage imageNamed:IS_IPHONE_4?@"callus_i4":@"callus_i5"]];
    
    UIImage * myaccount=[UIImage imageNamed:IS_IPHONE_4? @"myaccount_i4":@"myaccount_i5"];
    UIImage * request = [UIImage imageNamed:IS_IPHONE_4? @"requestdelivery_i4":@"requestdelivery_i5"];
    UIImage * promotion = [UIImage imageNamed:IS_IPHONE_4? @"promotion_i4":@"promotion_i5"];
    UIImage * newuser = [UIImage imageNamed:IS_IPHONE_4? @"newuser_i4":@"newuser_i5"];
    UIImage * help = [UIImage imageNamed:IS_IPHONE_4? @"help_i4":@"help_i5"];
    
    
    
    //[self addButtonWithFrame:CGRectMake(60.0, 350.0, saveImg.size.width, saveImg.size.height) tag:301 title:@"" image:saveImg];
    
    //NSLog(@"Image size %@",NSStringFromCGSize(newuser.size));
    [self addButtonWithFrame:CGRectMake(IS_IPHONE_4?6.67: 5.0, 425.0*deviceFactor, myaccount.size.width, myaccount.size.height) tag:1217 title:@"" image:myaccount];
    [self addButtonWithFrame:CGRectMake(IS_IPHONE_4?69.34:68.0, 425.0 *deviceFactor, request.size.width, request.size.height) tag:1218 title:@"" image:request];
    [self addButtonWithFrame:CGRectMake(132.0, 425.0*deviceFactor, promotion.size.width, promotion.size.height) tag:1219 title:@"" image:promotion];
    [self addButtonWithFrame:CGRectMake(195.0, 425.0*deviceFactor, IS_IPHONE_4?56: newuser.size.height, newuser.size.height) tag:1220 title:@"" image:newuser];
    [self addButtonWithFrame:CGRectMake(257.0, 425.0*deviceFactor, help.size.width, help.size.height) tag:1221 title:@"" image:help];
    
    //[self addLabelWithFrame:CGRectMake(20.0, 40.0, 80.0, 20.0) tag:121 title:@"Everest"];
    
    //[self addButtonWithFrame:CGRectMake(80.0, 300.0, 160.0, 40.0) tag:1214 title:@"Exit"];

	// Do any additional setup after loading the view, typically from a nib.
}

-(void)addLabelWithFrame:(CGRect)frame tag:(int)tag title:(NSString *)title
{
    UILabel* label = [[UILabel alloc] initWithFrame: frame];
    [label setText: title];
    label.tag =tag;
    [label setTextColor: [UIColor blackColor]];
    [self.view addSubview: label];
}
-(void)addButtonWithFrame:(CGRect)frame tag:(int)tag title:(NSString*)title image:(UIImage *)image
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self
               action:@selector(buttonAction:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:title forState:UIControlStateNormal];
    button.frame = frame;
    button.tag=tag;
    UIImage * buttonimg = image;
    [button setBackgroundImage:buttonimg forState:UIControlStateNormal];
    //[UIButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    //button.layer.borderColor=[[UIColor blackColor]CGColor];
    //button.layer.borderWidth= 1.0f;
}


-(IBAction)buttonAction:(UIButton*)sender
{
    if (sender.tag==1212)
    {
        NewUserViewController * newuser = [[NewUserViewController alloc]init];
        [self.navigationController pushViewController:newuser animated:YES];
    }
    else if (sender.tag == 1214)
    {
//        ContactViewController * contact = [[ContactViewController alloc]init];
//        [self.navigationController pushViewController:contact animated:YES];

        NSString *phoneNumber = @"+919509889734"; // dynamically assigned
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@", phoneNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    else if (sender.tag ==1213)
    {
        RequestDeliveryViewController * request = [[RequestDeliveryViewController alloc]init];
        [self.navigationController pushViewController:request animated:YES];
    }
    else if (sender.tag ==1215)
    {
        CatalogueViewController * catalogue = [[CatalogueViewController alloc]init];
        [self.navigationController pushViewController:catalogue animated:YES];
    }
    else if (sender.tag ==1216)
    {
        MyAccountsViewController * account = [[MyAccountsViewController alloc]init];
        [self.navigationController pushViewController:account animated:YES];
    }
    else if (sender.tag ==1217)
    {
        PromotionsViewController * promo = [[PromotionsViewController alloc]init];
        [self.navigationController pushViewController:promo animated:YES];
    }
    else if (sender.tag ==1218)
    {
        RequestDeliveryViewController * request = [[RequestDeliveryViewController alloc]init];
        [self.navigationController pushViewController:request animated:YES];
    }
    else if (sender.tag ==1219)
    {
        PromotionsViewController * promo = [[PromotionsViewController alloc]init];
        [self.navigationController pushViewController:promo animated:YES];
    }
    else if (sender.tag ==1220)
    {
        NewUserViewController * newuser = [[NewUserViewController alloc]init];
        [self.navigationController pushViewController:newuser animated:YES];
    }
    else if (sender.tag ==1221)
    {
        HelpViewController * help = [[HelpViewController alloc]init];
        [self.navigationController pushViewController:help animated:YES];
    }
        //NSLog(@"Hello Old");
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

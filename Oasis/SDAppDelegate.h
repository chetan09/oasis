//
//  SDAppDelegate.h
//  Oasis
//
//  Created by Ravi kumar on 08/11/14.
//  Copyright (c) 2014 FunkiTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
